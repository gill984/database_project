-- first truncate the data in FACULTY
TRUNCATE TABLE FACULTY;

LOAD DATA INFILE 'Faculty Table.csv' INTO TABLE FACULTY
	FIELDS TERMINATED BY ',' LINES STARTING BY '",,",';

-- repeat this load data for all the other schemas

-- test that everything was input correctly

SELECT * FROM default_schema.FACULTY;


