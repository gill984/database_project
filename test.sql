create table Faculty( 
	SSN int, 
	name char(30) , 
	lastName char(30), 
	phone int, 
	gender char, 
	dob DATE, 
	address char(75), 
	salary int,
	primary key(SSN) 
);

create table Administrator(
	role char(20),
	SSN int,
	primary key( SSN )
);

create table Teacher(
	SSN int,
	primary Key( SSN )
);

create table Staff(
	SSN int,
	role char(20),
	primary key( SSN )
);


create table Student(
	id int,
	name char(30),
	lastName char(30),
	gender char,
	dob DATE,
	address char(75),
	grade char,
	gpa char,
	primary key(id)
);

create table Course(
	id int,
	teacherSSN int,
	name char(30),
	grade char,
	semester char,
	theYear int,
	primary key(id, semester, theYear)
);

create table Enroll(
	studentId int,
	courseId int,
	courseSemester char,
	courseYear int,
	courseGrade char,
	primary key( studentId, courseId, courseSemester, courseYear, teacherSSN )
);

